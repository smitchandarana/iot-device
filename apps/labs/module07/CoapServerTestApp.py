'''
Created on Nov 1, 2019

@author: Smit2
'''
from labs.module07.CoapServerConnector import CoapServerConnector

#initialize the coap server connector
server = CoapServerConnector()
#start the server
server.start()
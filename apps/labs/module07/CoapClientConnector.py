'''
Created on Nov 1, 2019

@author: Smit2
'''

from coapthon.client.helperclient import HelperClient

class CoapClientConnector():
    '''
    connects to the CoAP server
    '''
    
    def __init__(self, host, port, path):
        '''
        constructor
        @param host: ip/domain of the server
        @param port: port number to connect 
        @param path: resource uri  
        '''
        self.host = host
        self.port = port
        self.path = path
        self.client = HelperClient(server=(host, port))
    
    def ping(self):
        '''
        function to ping the server
        '''    
        self.client.send_empty("")
        
    def get(self):
        '''
        function to GET request
        '''
        response = self.client.get(self.path)
        print(response.pretty_print())
    
    def post(self, jsonData):
        '''
        function to POST request
        @param jsonData: message/data to POST
        '''
        response = self.client.post(self.path, jsonData)
        print(response.pretty_print())
    
    def put(self, jsonData):
        '''
        Wrapper method for the PUT action
        @param jsonData: meassge or data to PUT 
        '''
        response = self.client.put(self.path, jsonData)
        print(response.pretty_print())
        
    def delete(self):
        '''
        function to DELETE request
        ''' 
        response = self.client.delete(self.path)
        print(response.pretty_print())
        
    def stop(self):
        '''
        function to stop the coap client
        '''  
        self.client.stop()     
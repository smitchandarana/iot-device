'''
Created on Nov 1, 2019

@author: Smit2
'''
from coapthon.server.coap import CoAP
from labs.module07.TempResourceHandler import TempResourceHandler
from labs.common.ConfigUtil import ConfigUtil
from labs.common.ConfigConst import ConfigConst

class CoapServerConnector(CoAP):
    '''
    listens to coap client
    '''

    def __init__(self):
        '''
        Constructor
        @param config:instance variable for ConfigUtil class
        @param host: ip/domain of the server
        @param port: port number to connect to    
        '''
        self.config = ConfigUtil()
        self.config.loadConfig()
        self.host = self.config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.HOST_KEY)
        self.port = int(self.config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.PORT_KEY))
        CoAP.__init__(self, (self.host, self.port))
        self.add_resource('temperature/', TempResourceHandler())  # Add Temperature resource while initializing server
        
    def start(self):
        '''
        start the server
        '''
        try:
            self.listen(10)
        except KeyboardInterrupt:
            print("Server Shutdown")
            self.close()
            print("Exiting...")
        finally:
            self.close()

'''
Created on Nov 1, 2019

@author: Smit2
'''
from labs.module07.CoapClientConnector import CoapClientConnector
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from labs.common.ConfigUtil import ConfigUtil
from labs.common.ConfigConst import ConfigConst

#instance variable for COnfigUtil class
config = ConfigUtil()
#loads the config file
config.loadConfig()
#instance variable for DataUtil class
data = DataUtil()
#ip/domain of the server 
host = config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.HOST_KEY)
#port number to connect
port = int(config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.PORT_KEY))
#resource uri
path = 'temperature'
#instance variable for SensorData class
sensorData = SensorData()
#add new sensordata value
sensorData.addValue(10.00)
#call the Coapclient connector
coapClient = CoapClientConnector(host, port, path)
#ping request
coapClient.ping()
#get request
coapClient.get()  
#post request
coapClient.post(data.SensordataToJson(sensorData))  
#add new value to sensor data
sensorData.addValue(5.00)
#put request
coapClient.put(data.SensordataToJson(sensorData))  
#delete request
coapClient.delete()  
#stop the Coap client
coapClient.stop()


'''
Created on Nov 1, 2019

@author: Smit2
'''
from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from coapthon.resources.resource import Resource
from coapthon import defines
from coapthon.messages import request


class TempResourceHandler(Resource):    
    '''
    Responds to GET, PUT, DELETE, POST request
    '''
    
    def __init__(self, name="TempResourceHandler", coap_server=None):
        '''
        constructor
        @param payload: stores the request response
        @param sensorDara:instance variable for SensorData class
        @param dataUtil: instance varible for DataUtil class   
        '''
        super(TempResourceHandler, self).__init__(name, coap_server)
        self.payload = "TempResourceHandler"
        self.sensorData = None
        self.dataUtil = DataUtil()
    
    def render_GET_advanced(self, request, response):
        '''
        responds to a GET request on the resource    
        Returns:response - CoAP response
        '''
        if(self.sensorData == None):
            response.code = defines.Codes.NOT_FOUND.number
            response.payload = (defines.Content_types["text/plain"], "Object needs to be initialized")
        else:
            response.code = defines.Codes.CONTENT.number
            print("\nReceived CoAP GET Sensor Data :\n" + str(self.sensorData))
            response.payload = (defines.Content_types["application/json"], self.dataUtil.toJsonFromSensorData(self.sensorData))     
        return self, response
                
    def render_POST_advanced(self, request, response):
        '''
        responds to a POST request on the resource    
        Returns:response - CoAP response
        '''   
        if(self.sensorData != None):
            response.code = defines.Codes.BAD_REQUEST.number
            response.payload = (defines.Content_types["text/plain"], "Object already exists")
        else:
            jsonData = request.payload
            print("\nReceived CoAP server POST in JSON AFTER format :\n" + str(jsonData))
            self.sensorData = SensorData()
            self.sensorData = self.dataUtil.JsonToSensorData(jsonData)
            print("\nReceived CoAP server POST in Sensor Data AFTER format :\n" + str(self.sensorData))
            print("\nConverted CoAP server POST Sensor Data into JSON FINAL format :\n" + self.dataUtil.SensordataToJson(self.sensorData))    
            response.code = defines.Codes.CREATED.number
            response.payload = (defines.Content_types["text/plain"], "Object created successfully")    
        return self, response
        
    def render_PUT_advanced(self, request, response):
        '''
        responds to a PUT request on the resource            
        Returns:response - CoAP response
        '''
        if(self.sensorData == None):
            response.code = defines.Codes.BAD_REQUEST.number
            response.payload = (defines.Content_types["text/plain"], "Object needs to be initialized")
        else:
            jsonData = request.payload
            print("\nReceived CoAP server PUT in JSON AFTER format :\n" + jsonData)
            self.sensorData = self.dataUtil.JsonToSensorData(jsonData)
            print("\nReceived CoAP server PUT in Sensor Data AFTER format :\n" + str(self.sensorData))
            print("\nConverted CoAP server PUT Sensor Data into JSON FINAL format :\n" + self.dataUtil.SensordataToJson(self.sensorData))                 
            response.code = defines.Codes.CHANGED.number
            response.payload = (defines.Content_types["text/plain"], "Object changed successfully")    
        return self, response
        
    def render_DELETE_advanced(self, request, response):
        '''
        responds to a PUT request on the resource            
        Returns: response - CoAP response
        '''
        if(self.sensorData == None):
            response.code = defines.Codes.BAD_REQUEST.number
            response.payload = (defines.Content_types["text/plain"], "Object already deleted")
        else:
            self.sensorData = None  # Set SensorData object to null     
            response.code = defines.Codes.DELETED.number
            response.payload = (defines.Content_types["text/plain"], "Object deleted successfully")    
        return self, response 
            

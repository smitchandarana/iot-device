'''
Created on Sep 21, 2019

@author: smit2u
'''
from labs.module01.SystemMemUtilTask import *
from labs.module01.SystemCpuUtilTask import *
from time import sleep
import logging
from threading import Thread
'''Using Thread to get the information'''
class SystemPerformanceAdaptor():
    def __init__(self):
        
        Thread.__init__(self)
        self.enableAdaptor = True
        self.rateInSec=9
'''The data is collected using the getDataFromSensor() method'''        
def run(self):
        while True:
            if self.enableAdaptor:
                cpuUtil = SystemCpuUtilTask.getDataFromSensor(self)
                memUtil = SystemMemUtilTask.getDataFromSensor(self)
                perfData = 'cpuUtil=' + str(cpuUtil) + '; ' + 'memUtil=' + str(memUtil)
            
                logging.info(perfData)
                sleep(self.rateInSec)

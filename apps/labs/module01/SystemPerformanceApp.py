'''
Created on Sep 21, 2019

@author: smit2
'''
import logging
from time import sleep
from labs.module01 import SystemPerformanceAdaptor
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
logging.info("Starting system performance app daemon thread...")
# If SystemPerformanceAdaptor extends from threading.Thread...
systemPerfAdaptor = SystemPerformanceAdaptor.SystemPerformanceAdaptor()
systemPerfAdaptor.daemon = True
systemPerfAdaptor.run()
while (True):
    sleep(5)
pass

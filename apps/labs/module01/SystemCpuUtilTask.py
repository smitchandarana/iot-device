'''
Created on Sep 21, 2019

@author: smit2
'''
''' Using psutil to get the CPU Usage data '''
import psutil
class SystemCpuUtilTask:
    def getDataFromSensor(self):
        return psutil.cpu_percent(interval=1)
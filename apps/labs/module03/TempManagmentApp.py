'''
Created on Oct 5, 2019

@author: smit2
'''

from labs.module03 import TempSensorAdaptor

# instance of temperature Emulator created with threshold value of 3
simulator = TempSensorAdaptor.TempSensorEmulator(3)
#Triggring the Simultor to send the message
simulator.setEmulator(True)
# Start Function of the thread
simulator.start()
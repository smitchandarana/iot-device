'''
Created on Oct 13, 2019

@author: smit2
'''

from labs.common import ActuatorData
from labs.module03 import SenseHatLedActivator
class TempActuatorEmulator(object):
    '''
    Actuator emulator, process the actuator data
    '''
    actuatorData = None
    senseHat = None
    

    def __init__(self):
        '''
        Constructor
        @param actuatorData: instance of actuator data class
        @param senseHat: instance of sensehatledactivator class   
        '''
        self.actuatorData = ActuatorData.ActuatorData()
        self.senseHat = SenseHatLedActivator.SenseHatLedActivator()
        #start the sensehat thread
        self.senseHat.start();
        
        
    def processMessage(self,actuatorData):
        '''
        compare actuator data recieved and according show the message on the sensehat
        '''
        #compare the actuator data recieved and actuator instance of this class
        if(self.actuatorData != actuatorData):
            #if the actuator value is greater than zero i.e temperature is above nominal show decrease message
            if(actuatorData.getValue() > 0):
                #set the enableled flag to true
                self.senseHat.setEnableLedFlag(True)
                #display message on sensehat
                self.senseHat.setDisplayMessage('decrease the temperature by :' + str(actuatorData.getValue()))
                #print('       \ndecrease the temperature by:' + str(actuatorData.getValue()))
                #update the actuatordata instance for this class
                self.actuatorData.updateData(actuatorData)
            #if the value less than zero increase the temperature
            elif(actuatorData.getValue() < 0):
                #set the enableled flag to true
                self.senseHat.setEnableLedFlag(True)
                self.senseHat.setEnableLedFlag(True)
                self.senseHat.setDisplayMessage('increase the temperature by :' + str(actuatorData.getValue()));
                #print('       \nincrease the temperature by:' + str(abs(actuatorData.getValue())))
                self.actuatorData.updateData(actuatorData)
        
'''
Created on Oct 5, 2019

@author: smit2
'''


from time import sleep
from sense_hat import SenseHat
import threading

class SenseHatLedActivator(threading.Thread):
    '''
    Displays message on SenseHat 
    @param enableLed: boolean to set the led flag
    @param rateInSec: Thread sleep duration
    @param sh: SensorHat instance
    @param displayMsg: The message to display     
    '''
    enableLed = False
    rateInSec = 1
    rotateDeg = 270
    sh = None
    displayMsg = None


    def __init__(self):
        '''
        Constructor
        '''
        super(SenseHatLedActivator, self).__init__()
        #check if thread sleep duration in greater than zero
        if self.rateInSec > 0:
            #set the sleep duration
            self.rateInSec = self.rateInSec
        #check if rotation greater than zero
        if self.rotateDeg >= 0:
            #set the rotation
            self.rotateDeg = self.rotateDeg
            #Sensehat instance
            self.sh = SenseHat()
            self.sh.set_rotation(self.rotateDeg)
            
            
    def run(self):
        '''
        function called when thread starts
        '''
        while True:
            #check enableLed flag
            if self.enableLed:
                #check on display message
                if self.displayMsg != None:
                    #show the display message
                    self.sh.show_message(str(self.displayMsg))
                else:
                    self.sh.show_letter(str('R'))
                    #hold duration 
                sleep(self.rateInSec)
                #clear the display
                self.sh.clear()
            sleep(self.rateInSec)
            
            
    def getRateInSeconds(self):
        return self.rateInSec
    
    
    def setEnableLedFlag(self, enable):
        self.sh.clear()
        self.enableLed = enable
        
        
    def setDisplayMessage(self, msg):
        self.displayMsg = msg
            
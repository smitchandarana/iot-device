'''
Created on Oct 5, 2019

@author: smit2
'''
from time import sleep
import threading
import RPi.GPIO as GPIO

class SimpleLedActivator(threading.Thread):
    '''
    classdocs
    '''
    # boolean to enable led
    enableLed = False
    #thread sleep duration
    rateInSec = 30
    
    def __init__(self, rateInSec = 1):
        '''
        Constructor
        '''
        super(SimpleLedActivator,self).__init__()
        if(rateInSec > 0):
            self.rateInSec = rateInSec
        #set the mode of GPIO
        GPIO.setmode(GPIO.BCM)
        #set the pin and pin value 
        GPIO.setup(17, GPIO.LOW)
        
    def run(self):
        '''
        thread start
        '''
        while True:
            # check the enable led flag
            if self.enableLed:
                #set the output value as high for GPIO pin 17
                GPIO.output(17, GPIO.HIGH)
                sleep(self.rateInSec)
                #set the output value as low for GPIO pin 17
                GPIO.output(17, GPIO.LOW)
        sleep(self.rateInSec)
        
        
    def getRateInSeconds(self):
        return self.rateInSec
    
    
    def setEnableLedFlag(self, enable):
        GPIO.setup(17, GPIO.LOW)
        self.enableLed = enable
        
        
        
        
    
    

'''
Created on Sep 28, 2019

@author: smit2
'''
#Creating a method to generate random values for Temperature. It calculates Average Temp.
import threading
from labs.common import SensorData
from labs.module02 import SmtpClientConnector
from labs.common import ConfigUtil
from labs.common import ConfigConst
from time import sleep
import random
class TempSensorEmulator(threading.Thread):
    
    config = ConfigUtil.ConfigUtil()
    
    
    def __init__(self,alertdiff):
        '''
        constructor
        @param enableEmulator: boolean to enable the emulator
        @param sensorData: instance of sensorData class
        @param connector: instance of smtpClientConnector class
        @param timeInterval: the duration after which new temp value is generated
        @param alertDiff: the threshold value for sending alert message
        @param lowValue: lowest value for the temperature
        @param highValue: highest value for the temperature
        @param curTemp: current value for the temperature      
        '''
        threading.Thread.__init__(self)
        self.enableEmulator = False
        self.sensorData = SensorData.SensorData()
        self.connector =  SmtpClientConnector.SmtpClientConnector()
        self.timeInterval = int(self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.POLL_CYCLES_KEY))
        self.alertDiff = alertdiff
        self.lowValue = 0
        self.highValue = 30
        self.curTemp = 0
        
            
    def setEmulator(self,boolean):
        '''
        set the boolean value to start the emulator
        '''
        self.enableEmulator = boolean
        
        
    def run(self):
        '''
        this function is called when thread starts
        '''
        threading.Thread.run(self)
        while True:
            if(self.enableEmulator):
                self.curTemp = random.uniform(float(self.lowValue),float(self.highValue))
                self.sensorData.addValue(self.curTemp)
                print('-----------------------------------------------')
                print('new Temperature reading:')
                print(str(self.sensorData))
                if(abs(self.curTemp - self.sensorData.getAvgValue())>= self.alertDiff):
                    print('\n current temperature exceeds average by ' + str(self.alertDiff) + ' Triggering alert message')
                    self.connector.publishMessage('Temperature Alert message', self.sensorData)
                sleep(self.timeInterval)
                    
            
            
        
        
'''
Created on Oct 22, 2019
@author: Smit2
'''

from labs.module06 import MqttClientConnector

#instance variable for MqttClientConnector
connector = MqttClientConnector.MqttClientConnector()

#Connecting to broker
connector.connect(None, None)

#Publishing message to topic mytest
connector.publishMessage("mytest", connector.message(), 2)

#Disconnect
connector.disconnect()


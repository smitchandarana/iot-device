'''
Created on Oct 22, 2019
@author: Smit2
'''

from labs.module06 import MqttClientConnector
from time import sleep

#MqttClientConnector instance
connector = MqttClientConnector.MqttClientConnector()

#Connecting to broker
connector.connect(None, None)

#subscribing to a topic
connector.subscibetoTopic("test")

#pause
sleep(60)

#unsubscribe from topic
connector.unsubscibefromTopic("test")

#disconnecting from broker
connector.disconnect()
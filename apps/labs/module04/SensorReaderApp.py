'''
Created on Oct 11, 2019

@author: smit2
'''
import sys
from time import sleep
#print(sys.path)
#sys.path.append('/home/pi/workspace/iot-device/apps')
#print(sys.path)
from labs.module03 import TempSensorAdaptor
from labs.module04 import I2CSenseHatAdaptor
from labs.module04 import SenseHatDeviceAdaptor
# instance of temperature Emulator created with threshold value of 3
#simulator = TempSensorAdaptor.TempSensorEmulator(3)
i2cSenseHatAdaptor = I2CSenseHatAdaptor.I2CSenseHatAdaptor()
# I2CSenseHatAdaptor.start()
#Triggring the Simultor to send the message
i2cSenseHatAdaptor.enableEmulator=(True)
# Start Function of the thread
i2cSenseHatAdaptor.start()
i=0;
while True:
    
    i2cSenseHatAdaptor.run()
    #getting value from the SenseHatDeviceAdaptor
    sensehatvalue = SenseHatDeviceAdaptor.SenseHatDeviceAdaptor() 
    print("End Of Cycle :", i)      
    sleep(10)
    i=i+1



'''
Created on Oct 11, 2019

@author: smit2
'''

from sense_hat import SenseHat

class SenseHatDeviceAdaptor():
    

# Reading the Data from the SenseHat API
    def __init__(self):
        self.hat = SenseHat()
        
    
        self.H = self.hat.get_humidity()
        self.T = self.hat.get_temperature()
        
        #Printing the data from the API
        print("Data Gathered from the SenseHat API")
        print("Humididty is:", self.H)
        print("Temperature is:", self.T)
        #print("END OF THE TASK")

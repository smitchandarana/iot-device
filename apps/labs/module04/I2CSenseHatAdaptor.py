'''
Created on Oct 11, 2019

@author: smit2
'''
import smbus
import threading
from time import sleep

import sys
sys.path.append('/home/pi/workspace/iot-device/apps')

from labs.common import ConfigUtil
from labs.common import ConfigConst
from sense_hat import SenseHat
enableEmulator = False
i2cBus = smbus.SMBus(1)  # Use I2C bus No.1 on Raspberry Pi3 +
enableControl = 0x2D
enableMeasure = 0x08
accelAddr = 0x1C         # address for IMU (accelerometer)
magAddr = 0x6A           # address for IMU (magnetometer)
pressAddr = 0x5C         # address for pressure sensor
humidAddr = 0x5F         # address for humidity sensor
begAddr = 0x28
totBytes = 6
H_OUT_L = 0x28
H_OUT_M = 0x29
H0_rH_x2 = 0x30
H1_rH_x2 = 0x31
H0_TO_OUT_L = 0x36
H0_TO_OUT_M = 0x37
H1_TO_OUT_L = 0x3A
H1_TO_OUT_M = 0x3B
H_T_OUT = 0
#Getting value for H0_rH and H1_rH



DEFAULT_RATE_IN_SEC = 5

class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        self.config = ConfigUtil.ConfigUtil(ConfigConst.DEFAULT_CONFIG_FILE_NAME)
        self.config.loadConfig()
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()

    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses...")
        i2cBus.write_byte_data(accelAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(magAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(pressAddr, enableControl, enableMeasure)
        i2cBus.write_byte_data(humidAddr, enableControl, enableMeasure)
        
#data = i2cBus.read_i2c_block_data({sensor address}, {starting read address}, {number of bytes})
#Methods Below are commands that can read data from sensors

#Accelerometer Data
    def displayAccelerometerData(self):
        accel_only = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        print("Accelerometer: ")
        print(accel_only)

#Pressure Data
    def displayPressureData(self):
        pressure = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        print("Pressure: ")
        print(pressure)
    #Converting the block data into readable Humidity     
    def humidity(self):
        self.H0_rh = (i2cBus.read_byte_data(humidAddr, H0_rH_x2))/2.0
        self.H1_rh = (i2cBus.read_byte_data(humidAddr, H1_rH_x2))/2.0
        # Value of H0_TO_OUT
        self.H0_TO_OUT_L= (i2cBus.read_byte_data(humidAddr, H0_TO_OUT_L))
        self.H0_TO_OUT_H= (i2cBus.read_byte_data(humidAddr, H0_TO_OUT_M))
        self.H0_TO_OUT= self.H0_TO_OUT_L + (self.H0_TO_OUT_H<<8)
       # print(self.H0_TO_OUT)
        #Reading value of H1_TO_OUT
        self.H1_TO_OUT_L= (i2cBus.read_byte_data(humidAddr, H1_TO_OUT_L))
        self.H1_TO_OUT_H= (i2cBus.read_byte_data(humidAddr, H1_TO_OUT_M))
        self.H1_TO_OUT= self.H1_TO_OUT_L + (self.H1_TO_OUT_H<<8)
       # print(self.H0_TO_OUT)
        #Reading Value of H_OUT
        self.H_T_OUT_L = (i2cBus.read_byte_data(humidAddr, H_OUT_L))
        self.H_T_OUT_M = (i2cBus.read_byte_data(humidAddr, H_OUT_M))
        self.H_T_OUT= self.H_T_OUT_L + (self.H_T_OUT_M<<8)
         
       # print(self.H_T_OUT)
        #Calculating the value of humidity in float 
        hrH = (((self.H1_rh - self.H0_rh)*(H_T_OUT-self.H0_TO_OUT))/(self.H1_TO_OUT - self.H0_TO_OUT)) + self.H0_rh
        print("Humidity after converting from block Data:", hrH)
        
        
#Humidity Data
    def displayHumidityData(self):
        humidity = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        print("Humidity: ")
        print(humidity)

#Magnetometer Data
    def displayMagnetometerData(self):
        orientation = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        print("Orientation: ")
        print(orientation)
       # print("------------------------")
        
#Reading Data from the I2CBus to get Humidity in Human Readable Float Value


#Run methods above and print the data on the console, the inteval is 5 seconds.
    def run(self):
        #print("try")
       # while True:
#             self.enableEmulator =True
#sleep(self.rateInSec)
        if self.enableEmulator:
        # NOTE: you must implement these methods
            print("Block Data I2CBUS")
            self.displayAccelerometerData()
            self.displayMagnetometerData()
            self.displayPressureData()
            self.displayHumidityData()
            print("------------------------")
            self.humidity()
          
            
            
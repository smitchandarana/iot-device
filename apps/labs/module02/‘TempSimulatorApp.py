'''
Created on Sep 28, 2019

@author: smit2
'''
from labs.module02 import TempSensorEmulator

# instance of temperature Emulator created with threshold value of 3
simulator = TempSensorEmulator.TempSensorEmulator(3)
#Triggring the Simultor to send the message
simulator.setEmulator(True)
# Start Function of the thread
simulator.start()